#Exercicio CI/CD

Utilize o código da demo count de terraform para o exercício.
Crie um novo repositório no gitlab e suba somente o código da demo count nele.
Adicione estado remoto do terraform no repositório.
Adicione um pipeline que irá validar o código e fazer o deploy dos recursos terraform.
Ao final o pipeline deve ter 3 etapas, as maquinas EC2 da demo count devem estar disponiveis e um relatório sobre testes e melhorias na configuração do terraform deve estar disponivel no pipeline do gitlab.
